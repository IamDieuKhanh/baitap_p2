// lấy danh sách sản phẩm
function getProduct() {
  return axiosClient({
    method: "GET",
    url: "products",
  });
}

function addProduct(newProduct) {
  return axiosClient({
    method: "POST",
    url: "products",
    data: newProduct,
  });
}

function deleteProduct(id) {
  return axiosClient({
    method: "DELETE",
    url: `products/${id}`,
  });
}