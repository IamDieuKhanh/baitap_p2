const Product = function (
  //id,
  name,
  image,
  description,
  price,
  inventory,
  rating,
  type
) {
  //this.id = id;
  this.name = name;
  this.image = image;
  this.description = description;
  this.price = price;
  this.inventory = inventory;
  this.rating = rating;
  this.type = type;

  this.render = function () {
    return `
      <div class="col-3">
        <div class="card p-2">
          <img style="height:280px;" src="${this.image}"
          class="w-100" alt="">
          <p id=""nameID>Tên: ${this.name}</p>
          <p id="descriptionID">Mô tả: ${this.description}</p>
          <p id="priceID" style="color: blue;">Giá: ${this.price}</p>
          <p id="inventoryID">Số lượng: ${this.inventory}</p>
          <p id="ratingID">Đánh giá: ${this.rating}</p>
          <p id="typeID">Loại: ${this.type}</p>
          <button class="btn btn-success" onclick="addProductToCard('${this.id}')">Cart</button>
        </div>
      </div>
    
    `;
  };
};
